require 'nokogiri' #biblioteca necessária para o processamento de paginas html, necessita instalação previa (gem install nokogiri)
require 'open-uri' #biblioteca necessária para o processamentos de endereços de internet, necessita instalação previa (gem install open-uri)

url = "https://mirrors.fy1m.com/h_tools/" #pagina a ser processada
extensao = ".msi" #tipo de arquivo a ser baixado da pagina

page = Nokogiri::HTML(URI.open(url).read) #a variavel page vai receber o conteudo html da pagina a ser processada
links = page.xpath('//@href').map(&:value) #a variavel links vai receber um array com o conteudo de todas as tags que possuirem o atributo 'href'

for i in 0..links.size do #laço for que vai iterar sobre o array links
   unless links[i].nil? then #verifica se o conteudo da variavel links iterado no momento não é nulo
      if links[i].include?(extensao) then #limita para que apenas arquivos con a extensao informada sejam baixados e evita que outros conteudos quebrem o programa
         puts "Processando o arquivo numero "+i.to_s+": "+links[i] #imprime o nome do arquivo no terminal
         URI.open(url+links[i]) do |arquivo| #é necessário informar o endereço inteiro do arquivo a ser baixado, por isso a concatenação das variaveis url e links.
           File.open("ruby_"+links[i], "wb") do |file| #comando que cria um arquivo fisico, o primeiro parametro é o nome do arquivo e o segundo indica o modo de escrita, no caso 'wb' significa 'write binary'
             file.write(arquivo.read) #comando que inicia a leitura dos dados obtidos da página e grava no arquivo criado no passo anterior
           end
         end
      else
         puts "O arquivo numero "+i.to_s+" não possui a extensao \""+extensao+"\""
      end
   end
end